drop table if exists credit_items;
drop table if exists credit_tables;
drop table if exists debit_items;
drop table if exists debit_tables;
drop table if exists users;
drop table if exists items;

create table users (
	userid int not null auto_increment,
    username varchar(50),
    pwd varchar(50),
    primary key(userid)
);

insert into users values (1, "root", "admin");

create table credit_tables (
	tableid int not null auto_increment,
    tablename varchar(50),
    fk_user int,
    primary key(tableid),
    foreign key(fk_user) references users(userid)
);

create table debit_tables (
	tableid int not null auto_increment,
    tablename varchar(50),
    fk_user int,
    primary key(tableid),
    foreign key(fk_user) references users(userid)
);

create table credit_items (
	itemid int not null,
    itemname varchar(50),
    itemvalue float,
    ismonthly boolean,
    fk_credit_table int,
    primary key(itemid),
    foreign key(fk_credit_table) references debit_tables(tableid)
);

create table debit_items (
	itemid int not null,
    itemname varchar(50),
    itemvalue float,
    ismonthly boolean,
    fk_debit_table int,
    primary key(itemid),
    foreign key(fk_debit_table) references debit_tables(tableid)
);