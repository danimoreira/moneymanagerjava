package application;
	
import constants.Constants;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import root.RootCtrl;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader();
		
		loader.setLocation(getClass().getResource(Constants.ROOT_PATH));		
		
		BorderPane rootPane = loader.load();
		RootCtrl rootCtrl = loader.getController();
		
		Scene scene = new Scene(rootPane, 600, 480);		
		primaryStage.setTitle("Money Manager");
		primaryStage.setScene(scene);
		primaryStage.show();		
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
