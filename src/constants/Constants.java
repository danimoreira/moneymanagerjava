package constants;

public class Constants {
	public static final String ROOT_PATH = "/root/Root.fxml";
	public static final String LOGIN_PATH = "/login/Login.fxml";
	public static final String REGISTER_PATH = "/register/Register.fxml";
	public static final String MANAGER_PATH = "/manager/Manager.fxml";
	public static final String SETTINGS_PATH = "/settings/Settings.fxml";

	public static final String MENU_PATH = "/menu/Menu.fxml";
}
