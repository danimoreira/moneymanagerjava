package db;

import javax.persistence.EntityManagerFactory;

import db.item.ItemRepository;
import db.users.UserRepository;

public class RepositoryFactory {

	private static UserRepository userRepo;
	private static ItemRepository itemRepo;
	
	public static enum Action {
		getUserRepository {
			@SuppressWarnings("unchecked")
			UserRepository create(EntityManagerFactory factory) {
				if(userRepo == null)
					userRepo = new UserRepository(factory);
				
				return userRepo;
			}
		},
		getItemRepository {
			@SuppressWarnings("unchecked")
			ItemRepository create(EntityManagerFactory factory) {
				if(itemRepo == null)
					itemRepo = new ItemRepository(factory);
				
				return itemRepo;
			}
		};
		
		abstract <T extends IRepository> T create(EntityManagerFactory factory);		
	}	
}
