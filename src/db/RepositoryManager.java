package db;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import db.item.ItemRepository;
import db.users.UserRepository;

public class RepositoryManager {

	private static RepositoryManager INSTANCE = null;
	private EntityManagerFactory factory = null;
	
	
	private RepositoryManager() {
		initDbSession();
	}
	
	public static RepositoryManager getInstance() {
		if(INSTANCE == null)
			INSTANCE = new RepositoryManager();
		
		return INSTANCE;
	}
	
	private void initDbSession() {
		factory = Persistence.createEntityManagerFactory("MoneyManagerPU");
	}
	
	public UserRepository getUserRepository() {
		return RepositoryFactory.Action
				.getUserRepository.create(factory);
	}
	
	public ItemRepository getItemRepository() {
		return RepositoryFactory.Action
				.getItemRepository.create(factory);
	}
	
}
