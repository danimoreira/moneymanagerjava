package db.debit;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import db.item.DebitItemModel;
import db.users.UserModel;

@Entity
@Table(name="debit_table")
public class DebitTableModel {
	@Id
	@Column(name="tableid", unique=true)
	private int tableId;
	@Column(name="tablename", nullable=false)
	private String tableName;
	
	@OneToMany(mappedBy="table")
	private Set<DebitItemModel> debitItems;	
	@ManyToOne
	@JoinColumn(name="userid", nullable=false)
	private UserModel user;
	
	public int getTableId() {
		return tableId;
	}
	public void setTableId(int tableId) {
		this.tableId = tableId;
	}
	
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	
	public UserModel getUser() {
		return user;
	}
	public void setUser(UserModel user) {
		this.user = user;
	}
	
	public Set<DebitItemModel> getDebitItems() {
		return debitItems;
	}
	public void setDebitItems(Set<DebitItemModel> debitItems) {
		this.debitItems = debitItems;
	}
}
