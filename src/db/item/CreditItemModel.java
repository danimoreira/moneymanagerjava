package db.item;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import db.credit.CreditTableModel;

@Entity
@Table(name="credit_items")
public class CreditItemModel {
	@Id
	@Column(name="itemid", unique=true)
	private int itemId;
	@Column(name="itemname", nullable=false)
	private String itemName;
	@Column(name="itemvalue", nullable=false)
	private float itemValue;
	@Column(name="ismonthly", nullable=false)
	private boolean isMonthly;
	
	@ManyToOne
	@JoinColumn(name="tableid", nullable=false)
	private CreditTableModel table;

	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public float getItemValue() {
		return itemValue;
	}
	public void setItemValue(float itemValue) {
		this.itemValue = itemValue;
	}

	public boolean isMonthly() {
		return isMonthly;
	}
	public void setMonthly(boolean isMonthly) {
		this.isMonthly = isMonthly;
	}

	public CreditTableModel getTable() {
		return table;
	}
	public void setTable(CreditTableModel table) {
		this.table = table;
	}
}
