package db.item;

import javax.persistence.EntityManagerFactory;
import db.IRepository;

public class ItemRepository implements IRepository {

	private EntityManagerFactory factory;
	
	public ItemRepository (EntityManagerFactory factory) {
		this.factory = factory;
	}
	
	public void addItem() {
	}
	
	public void updateItem() {		
	}
	
	public void deleteItem() {		
	}
}
