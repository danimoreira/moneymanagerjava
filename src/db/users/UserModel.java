package db.users;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import db.credit.CreditTableModel;
import db.debit.DebitTableModel;

@Entity
@Table(name="users")
public class UserModel {
	@Id
	@Column(name="userid", unique=true)
	private int userId;
	@Column(name="username", nullable=false)
	private String username;
	@Column(name="pwd", nullable=false)
	private String pwd;
	
	@OneToMany(mappedBy="user")
	private Set<CreditTableModel> creditTables;
	@OneToMany(mappedBy="user")
	private Set<DebitTableModel> debitTables;
	
	public int getId() {
		return userId;
	}
	public void setId(int userId) {
		this.userId = userId;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
}
