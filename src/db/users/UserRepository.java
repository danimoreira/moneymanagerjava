package db.users;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import db.IRepository;

public class UserRepository implements IRepository {
	
	private EntityManagerFactory factory;
	
	public UserRepository(EntityManagerFactory factory) {
		this.factory = factory;
	}
	
	public void addUser(UserModel user) {
		EntityManager em = factory.createEntityManager();
		EntityTransaction transaction = null;
		
		try {
			transaction = em.getTransaction();
			transaction.begin();
			
			em.persist(user);
			
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null) {
                transaction.rollback();
            }
			
            e.printStackTrace();			
		} finally {
			em.close();
		}
	}
	
	public void updateUser() {		
	}
	
	public void deleteUser() {		
	}
	
	public UserModel getUser(String username) {
		EntityManager em = factory.createEntityManager();
		EntityTransaction transaction = null;
		UserModel user = null;
		
		try {
			transaction = em.getTransaction();
			transaction.begin();
			
			user = em.createQuery("SELECT u FROM UserModel u WHERE u.username = '" + username + "'", UserModel.class).getSingleResult();
		
			transaction.commit();
		} catch(Exception e) {
			if (transaction != null) {
                transaction.rollback();
            }
			
            e.printStackTrace();
		} finally {
			em.close();
		}
		
		return user;
	}
}
