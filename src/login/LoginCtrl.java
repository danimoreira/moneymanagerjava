package login;

import java.net.URL;
import java.util.ResourceBundle;

import constants.Constants;
import db.RepositoryManager;
import db.users.UserModel;
import db.users.UserRepository;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import utils.AbstractCtrl;
import utils.DialogUtil;

public class LoginCtrl extends AbstractCtrl {
	private UserRepository userRepository = null;
	
	@FXML private TextField userField;
	@FXML private PasswordField passField;
	
	@FXML private Button loginBt;
	@FXML private Button registerBt;
	@FXML private Button quitBt;

	public LoginCtrl() {
		userRepository = RepositoryManager.getInstance().getUserRepository();
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		loginBt.setOnAction(e -> {
			String username = userField.getText();
			String password = passField.getText();
			
			UserModel user = userRepository.getUser(username);
			
			if(user != null && user.getPwd().equals(password)) {
				rootCtrl.initView(Constants.MANAGER_PATH);
			} else {
				DialogUtil.showErrorDialog("Incorrect credentials", 
						"The username or password is incorrect", 
						"The username or password provided are incorrect. Please check again the fields and try again.");
				
				userField.setText("");
				passField.setText("");
			}
		});
		
		registerBt.setOnAction(e -> {
			rootCtrl.initView(Constants.REGISTER_PATH);
		});
		
		quitBt.setOnAction(e -> {
			System.exit(0);
		});
	}

	@Override
	public void init() {}
}
