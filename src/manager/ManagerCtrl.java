package manager;

import java.util.ArrayList;
import java.util.List;

import constants.Constants;
import javafx.fxml.FXML;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.VBox;
import menu.MenuCtrl;
import utils.AbstractCtrl;

public class ManagerCtrl extends AbstractCtrl {
	@FXML private VBox creditBox;
	@FXML private VBox debitBox;
	
	private List<TitledPane> creditPaneList = new ArrayList<>();
	private List<TitledPane> debitPaneList = new ArrayList<>();
	
	@Override
	public void init() {
		rootCtrl.addMenuBar(Constants.MENU_PATH, MenuCtrl.class)
		.setMainCtrl(this);
	}
	
	public void addCreditTable(String identifier) {
		
	}
	
	public void addDebitTable(String identifier) {
		
	}
}
