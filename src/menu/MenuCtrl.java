package menu;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import manager.ManagerCtrl;
import utils.AbstractCtrl;
import utils.DialogUtil;

public class MenuCtrl extends AbstractCtrl {
	@FXML private Button addCreditTableBt;
	@FXML private Button addDebitTableBt;
	@FXML private Button addCreditGraphBt;
	@FXML private Button addDebitGraphBt;
	@FXML private Button settingsBt;
	
	private ManagerCtrl mainCtrl;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {		
		addCreditTableBt.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				String identifier = DialogUtil.addCreditTable();
				mainCtrl.addCreditTable(identifier);
			}
		});
		
		addDebitTableBt.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				String identifier = DialogUtil.addDebitTable();
				mainCtrl.addDebitTable(identifier);
			}
		});
		
		addCreditGraphBt.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

			}
		});
		
		addDebitGraphBt.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {

			}
		});
	}

	@Override
	public void init() {
	}
	
	public void setMainCtrl(ManagerCtrl mainCtrl) {
		this.mainCtrl = mainCtrl;
	}
}
