package register;

import java.net.URL;
import java.util.ResourceBundle;

import constants.Constants;
import db.RepositoryManager;
import db.users.UserModel;
import db.users.UserRepository;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import utils.AbstractCtrl;
import utils.DialogUtil;

public class RegisterCtrl extends AbstractCtrl {
	@FXML private TextField usernameField;
	@FXML private TextField pwdField;
	@FXML private TextField repeatPwdField;
	@FXML private Button registerBt;
	@FXML private Button cancelBt;
	
	private UserRepository userRepository = null;
	
	public RegisterCtrl() {
		userRepository = RepositoryManager.getInstance().getUserRepository();
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		registerBt.setOnAction(e -> {
			boolean isOk = true;
			
			if(!pwdField.getText().equals(repeatPwdField.getText())) {
				isOk = DialogUtil.showErrorDialog("Error", 
						"The program was not able to create the user", 
						"Password is not typed twice correctly");
			}
			
			if(isOk) {
				UserModel newUser = new UserModel();
				newUser.setUsername(usernameField.getText());
				newUser.setPwd(repeatPwdField.getText());
				
				userRepository.addUser(newUser);
				
				DialogUtil.showValidDialog("User added", 
						"The new user has been registered", 
						"The new user with username \"" + usernameField.getText() + "\" has been added correctly.");
				
				rootCtrl.initView(Constants.LOGIN_PATH);
			}
		});
		
		cancelBt.setOnAction(e -> {
			rootCtrl.initView(Constants.LOGIN_PATH);
		});
	}

	@Override
	public void init() {
		
	}

}
