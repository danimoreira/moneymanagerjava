package root;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import constants.Constants;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import utils.AbstractCtrl;

public class RootCtrl implements Initializable {
	@FXML private BorderPane rootPane;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initView(Constants.LOGIN_PATH);
	}

	public void initView(String fxmlUrl) {
		try {
			FXMLLoader loader = new FXMLLoader();
			
			loader.setLocation(getClass().getResource(fxmlUrl));
			Node content = loader.load();
			
			AbstractCtrl ctrl = loader.getController();
			ctrl.setRootCtrl(this);
			ctrl.init();
			
			rootPane.setCenter(content);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public <T> T addMenuBar(String fxmlUrl, Class<T> type) {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource(fxmlUrl));
		
		try {
			rootPane.setRight(loader.load());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return loader.getController();
	}
}