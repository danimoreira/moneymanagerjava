package utils;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.Initializable;
import root.RootCtrl;

public abstract class AbstractCtrl implements Initializable {
	protected RootCtrl rootCtrl;
	
	public void setRootCtrl(RootCtrl rootCtrl) {
		this.rootCtrl = rootCtrl;
	}
	
	public abstract void init();
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {}
}
