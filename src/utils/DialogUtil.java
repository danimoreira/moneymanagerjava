package utils;

import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextInputDialog;

public class DialogUtil {

	public static String addCreditTable () {
		TextInputDialog creditTableDialog = new TextInputDialog();
		creditTableDialog.setTitle("Add credit table");
		creditTableDialog.setHeaderText("Select an identifier for the table");
		creditTableDialog.setContentText("Identifier");
		
		Optional<String> identifier = creditTableDialog.showAndWait();
		
		if(identifier.isPresent() && !identifier.get().isEmpty())
			return identifier.get();
		else
			return null;
	}
	
	public static String addDebitTable () {
		TextInputDialog debititTableDialog = new TextInputDialog();
		debititTableDialog.setTitle("Add debit table");
		debititTableDialog.setHeaderText("Select an identifier for the table");
		debititTableDialog.setContentText("Identifier");
		
		Optional<String> identifier = debititTableDialog.showAndWait();
		
		if(identifier.isPresent() && !identifier.get().isEmpty())
			return identifier.get();
		else
			return null;
	}
	
	public static boolean showErrorDialog(String title, String headerText, String contentText) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(title);
		alert.setHeaderText(headerText);
		alert.setContentText(contentText);

		alert.showAndWait();
		
		return false;
	}
	
	public static boolean showValidDialog(String title, String headerText, String contentText) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(title);
		alert.setHeaderText(headerText);
		alert.setContentText(contentText);

		alert.showAndWait();
		
		return true;
	}
	
}
